import sbt.Keys.libraryDependencies

cancelable in Global := true

// We need to generate slightly different structure for IntelliJ in order to better support ScalaJS cross projects.
// idea.managed property is set by IntelliJ when running SBT (shell or import), idea.runid is set only for IntelliJ's
// SBT shell. In order for this technique to work, you MUST NOT set the "Use the sbt shell for build and import"
// option in IntelliJ's SBT settings.
val forIdeaImport = System.getProperty("idea.managed", "false").toBoolean && System.getProperty("idea.runid") == null

val commonsVersion = "1.34.19"
val scalajsDomVersion = "0.9.7"
val typesafeConfigVersion = "1.3.4"
val jsoupVersion = "1.7.3"
val udashVersion = "0.8.0-RC3"
val jettyVersion = "9.4.18.v20190429"
val guavaVersion = "27.1-jre"

inThisBuild(Seq(
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-deprecation",
    "-unchecked",
    "-feature",
    "-Xfuture",
    "-Xfatal-warnings"
  )
))

def mkSourceDirs(base: File, scalaBinary: String, conf: String): Seq[File] = Seq(
  base / "src" / conf / "scala",
  base / "src" / conf / s"scala-$scalaBinary",
  base / "src" / conf / "java"
)

def mkResourceDirs(base: File, conf: String): Seq[File] = Seq(
  base / "src" / conf / "resources"
)

def sourceDirsSettings(baseMapper: File => File) = Seq(
  unmanagedSourceDirectories in Compile ++=
    mkSourceDirs(baseMapper(baseDirectory.value), scalaBinaryVersion.value, "main"),
  unmanagedSourceDirectories in Test ++=
    mkSourceDirs(baseMapper(baseDirectory.value), scalaBinaryVersion.value, "test"),
  unmanagedResourceDirectories in Compile ++=
    mkResourceDirs(baseMapper(baseDirectory.value), "main"),
  unmanagedResourceDirectories in Test ++=
    mkResourceDirs(baseMapper(baseDirectory.value), "test"),
)

def sameNameAs(proj: Project) =
  if (forIdeaImport) Seq.empty
  else Seq(name := (name in proj).value)

val crawler = project.in(file("."))
  .enablePlugins(JavaServerAppPackaging)
  .settings(
    sourceDirsSettings(_ / "jvm"),
    libraryDependencies ++= Seq(
      "com.typesafe" % "config" % typesafeConfigVersion,
      "org.jsoup" % "jsoup" % jsoupVersion,
      "com.avsystem.commons" %% "commons-core" % commonsVersion,
      "io.udash" %% "udash-rest" % udashVersion,
      "org.eclipse.jetty" % "jetty-server" % jettyVersion,
      "org.eclipse.jetty" % "jetty-servlet" % jettyVersion,
      "com.google.guava" % "guava" % guavaVersion
    ),
    resourceGenerators in Compile +=
      (fullOptJS in LocalProject("js") in Compile).taskValue.map(af => Seq(af.data)),
    mainClass := Some("crawler.CrawlerServer"),
    sources in (Compile, doc) := Seq.empty,
  )

lazy val js: Project = project
  .enablePlugins(ScalaJSPlugin)
  .configure(p => if (forIdeaImport) p.dependsOn(crawler) else p)
  .settings(
    sameNameAs(crawler),
    sourceDirsSettings(_.getParentFile),
    libraryDependencies ++= Seq(
      "com.avsystem.commons" %%% "commons-core" % commonsVersion,
      "org.scala-js" %%% "scalajs-dom" % scalajsDomVersion,
      "io.udash" %%% "udash-rest" % udashVersion
    )
  )
