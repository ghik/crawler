package crawler

import com.avsystem.commons.misc.{Opt, Timestamp}
import io.udash.rest.{DefaultRestApiCompanion, GET, Path, RestDataCompanion}

import scala.concurrent.Future

case class Config(
  offerCount: Int,
  detectDuplicates: Boolean,
  baseUrl: String,
  pathBeforePage: String,
  pathAfterPage: String,
  urlParams: Map[String, String],
  rejectedTraits: Vector[String]
)
object Config extends RestDataCompanion[Config]

case class FlatOffer(
  id: String,
  url: String,
  title: String,
  added: Timestamp,
  price: Int,
  address: String,
  owner: Boolean,
  area: Int,
  rooms: Int,
  bathrooms: Int,
  parking: String,
  smoking: Boolean,
  animals: Boolean,
  description: String
)
object FlatOffer extends RestDataCompanion[FlatOffer]

trait CrawlerApi {
  def startCrawling(config: Config): Future[Int]

  @GET def next(@Path crawleId: Int): Future[Opt[FlatOffer]]
}
object CrawlerApi extends DefaultRestApiCompanion[CrawlerApi]
