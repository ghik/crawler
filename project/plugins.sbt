addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.28")
addSbtPlugin("com.heroku" % "sbt-heroku" % "2.1.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.21")
addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.1.0-M7")