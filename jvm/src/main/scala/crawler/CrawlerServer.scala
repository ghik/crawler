package crawler

import java.text.Normalizer.Form
import java.text.{Normalizer, SimpleDateFormat}
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

import com.avsystem.commons._
import com.avsystem.commons.misc.Timestamp
import com.google.common.io.ByteStreams
import io.udash.rest.RestServlet
import io.udash.rest.raw.HttpErrorException
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import org.eclipse.jetty.http.{HttpStatus, MimeTypes}
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.{ServletContextHandler, ServletHolder}
import org.jsoup.Jsoup

class Crawle(config: Config) {
  import config._

  def url(page: Int): String = {
    val urlQuery = urlParams.map({ case (k, v) => s"$k=$v" }).mkString("&")
    s"$baseUrl$pathBeforePage${if (page > 1) s"/page-$page" else ""}$pathAfterPage?$urlQuery"
  }

  val flatUrls: Iterator[String] = Iterator.from(1).flatMap { page =>
    println(s"Fetching ${url(page)}")
    Jsoup.connect(url(page)).get.getElementsByClass("tileV1").iterator.asScala
      .map(_.getElementsByClass("title").first.getElementsByClass("href-link").first.attr("href"))
      .map(rel => s"$baseUrl$rel")
  }

  val addedFormat = new SimpleDateFormat("dd/MM/yyyy")

  def extractInt(str: String): Option[Int] =
    Some(str.filter(_.isDigit)).filter(_.nonEmpty).map(_.toInt)

  def extractRooms(str: String): Option[Int] =
    if (str == "Kawalerka lub garsoniera") Some(1) else extractInt(str)

  def normalize(str: String): String =
    Normalizer.normalize(str.toLowerCase, Form.NFD)
      .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
      .replace('ł', 'l')
      .replaceAll("\\W", " ")
      .replaceAll("\\s+", " ")

  val titlesSeen = new MHashSet[String]
  val descriptionsSeen = new MHashSet[String]

  val offers: Iterator[FlatOffer] = flatUrls.flatMap { flatUrl =>
    try {
      val flatDoc = Jsoup.connect(flatUrl).timeout(10000).get

      val id = flatUrl.substring(flatUrl.lastIndexOf('/') + 1)
      val title = flatDoc.getElementsByClass("item-title").first.child(0).text.trim
      val price = flatDoc.getElementsByClass("price").first.child(0).child(0).text.filter(_.isDigit).toInt

      val attributes = flatDoc.getElementsByClass("attribute").iterator.asScala.map { attrDiv =>
        val name = attrDiv.getElementsByClass("name").first.text
        val value = attrDiv.getElementsByClass("value").first.text
        (name, value)
      }.toMap
      val added = attributes.get("Data dodania").map(addedFormat.parse).map(_.toTimestamp).getOrElse(Timestamp.Zero)
      val address = attributes.get("Adres").filter(_ != "Kraków, Małopolskie")
        .map(_.stripSuffix(", Kraków, Małopolskie")).orNull
      val owner = attributes.get("Do wynajęcia przez").contains("Właściciel")
      val area = attributes.get("Wielkość (m2)").map(_.toInt).getOrElse(0)
      val rooms = attributes.get("Liczba pokoi").flatMap(extractRooms).getOrElse(0)
      val bathrooms = attributes.get("Liczba łazienek").flatMap(extractInt).getOrElse(0)
      val parking = attributes.get("Parking").orNull
      val smoking = attributes.get("Palący").contains("Tak")
      val animals = attributes.get("Przyjazne zwierzakom").contains("Tak")

      val description = flatDoc.getElementsByClass("description").first.text

      val offer = FlatOffer(id, flatUrl, title, added, price, address, owner, area, rooms,
        bathrooms, parking, smoking, animals, description)

      val adjustedDescription = description
        .replaceAllLiterally("Grzegórzki, Śródmieście", "")

      val detectedNegTraits = for {
        negTrait <- rejectedTraits
        filteredString <- List(title, adjustedDescription, address).filter(_ != null).map(normalize)
        if filteredString.contains(negTrait)
      } yield negTrait

      if (detectedNegTraits.isEmpty && !titlesSeen.contains(title) && !descriptionsSeen.contains(description)) {
        if (detectDuplicates) {
          titlesSeen += title
          descriptionsSeen += title
        }

        println("Accepted")
        Some(offer)
      } else {
        println(s"Rejected because: ${detectedNegTraits.mkString(", ")}")
        None
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }.take(offerCount)

  def next: Opt[FlatOffer] = offers.nextOpt
}

object CrawlerApiImpl extends CrawlerApi {
  private val ids = new AtomicInteger
  private val crawles = new ConcurrentHashMap[Int, Crawle]

  def startCrawling(config: Config): Future[Int] = {
    val id = ids.getAndIncrement()
    crawles.put(id, new Crawle(config))
    Future.successful(id)
  }

  def next(crawleId: Int): Future[Opt[FlatOffer]] =
    crawles.get(crawleId) match {
      case null =>
        throw HttpErrorException(404, s"invalid crawle id: $crawleId")
      case crawle => crawle.next match {
        case Opt.Empty =>
          crawles.remove(crawleId)
          Future.successful(Opt.Empty)
        case v =>
          Future.successful(v)
      }
    }
}

object CrawlerServlet extends HttpServlet {
  override def doGet(req: HttpServletRequest, resp: HttpServletResponse): Unit = {
    val path = req.getPathInfo match {
      case "" | "/" => "/index.html"
      case p => p
    }
    getClass.getResourceAsStream(path).opt match {
      case Opt(stream) =>
        val contentType = path.lastIndexOf(".").opt.filter(_ >= 0)
          .flatMap(i => MimeTypes.getDefaultMimeByExtension(path.substring(i)).opt)
        contentType.foreach(resp.setContentType)
        ByteStreams.copy(stream, resp.getOutputStream)
      case Opt.Empty =>
        resp.sendError(HttpStatus.NOT_FOUND_404)
    }
  }
}

object CrawlerServer {
  def main(args: Array[String]): Unit = {
    val server = new Server(sys.env.get("PORT").fold(8080)(_.toInt))
    val context = new ServletContextHandler
    context.addServlet(new ServletHolder(RestServlet[CrawlerApi](CrawlerApiImpl)), "/api/*")
    context.addServlet(new ServletHolder(CrawlerServlet), "/*")
    server.setHandler(context)
    server.start()
    server.join()
  }
}
