package crawler

import com.avsystem.commons._
import com.softwaremill.sttp.SttpBackend
import io.udash.rest.{DefaultSttpBackend, SttpRestClient}
import org.scalajs.dom._
import org.scalajs.dom.html.Div

import scala.concurrent.Future
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("crawler")
object Crawler {
  final val DefaultConfig = Config(
    offerCount = 5,
    detectDuplicates = true,

    baseUrl = "https://www.gumtree.pl",
    pathBeforePage = "/s-mieszkania-i-domy-do-wynajecia/krakow/mieszkanie",
    pathAfterPage = "/v1c9008l3200208q0p1",
    urlParams = Map(
      "pr" -> ",1300",
      "nr" -> "10"
    ),

    rejectedTraits = Vector(
      "nowa huta", "nowej hucie", "nowej huty", "jagiellonski", "oswiecenia",
      "os piastow", "osiedle piastow", "osiedlu piastow",
      "os albertynski", "osiedle albertynski", "osiedlu albertynski",
      "os kolorow", "osiedle kolorow", "osiedlu kolorow",
      "os zielon", "osiedle zielon", "osiedlu zielon",
      "os krakowiakow", "osiedle krakowiakow", "osiedlu krakowiakow",
      "os goral", "osiedle goral", "osiedlu goral",
      "os stalow", "osiedle stalow", "osiedlu stalow",
      "os spoldzielcz", "osiedle spoldzielcz", "osiedlu spoldzielcz",
      "os ogrodow", "osiedle ogrodow", "osiedlu ogrodow",
      "os handlow", "osiedle handlow", "osiedlu handlow",
      "os wandy", "osiedle wandy", "osiedlu wandy", "kliny zacisze",
      "os na skarpie", "osiedle na skarpie", "osiedlu na skarpie",
      "os przy arce", "osiedle przy arce", "osiedlu przy arce",
      "os wysokie", "osiedle wysokie", "os wysokim", "osiedlu wysokim",
      "na stoku", "dywizjonu",
      "wielicka", "ruczaj", "prokocim", "duchack", "kurdwanow", "czyzyn", "mistrzejowic", "bienczyc", "lagiewnik",
      "falecki", "bartla", "grzegorzk", "debnik", "ruczj", "plaszow", "zakopiansk", "podgorz", "kobierzynsk",
      "obozowej", "obozowa", "zlocien", "dabie", "dabiu", "dabia", "biezanow", "pszona", "andersa",
      "kazimierz", "wroclaw", "lipska", "lipskiej", "sodowa", "sodowej", "łowienick", "babinskiego",
      "piaski nowe", "mieczykowa", "pulku lotniczego", "pulk lotniczy", "raciborsk", "luzycka", "wloska",
      "do remontu"
    )
  )


  @JSExport
  def main(): Unit = {
    implicit val backend: SttpBackend[Future, Nothing] = DefaultSttpBackend()
    val baseUri = window.location.href.stripSuffix(window.location.pathname)
    val crawlerApi: CrawlerApi = SttpRestClient[CrawlerApi](s"$baseUri/api")

    val mainDiv = window.document.getElementById("main").asInstanceOf[Div]

    crawlerApi.startCrawling(DefaultConfig).foreachNow { id =>
      def loop(): Unit = crawlerApi.next(id).foreachNow {
        case Opt(offer) =>
          val par = window.document.createElement("p")
          par.innerHTML = s"""<a href="${offer.url}">${offer.title}</a>"""
          mainDiv.appendChild(par)
          loop()
        case Opt.Empty =>
      }
      loop()
    }
  }
}
